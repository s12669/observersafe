package com.maciejsokol.ms;

import java.util.Date;

public class EnteredPinEvent {
    private Alarm alarm;
    private Date eventDate;

    public EnteredPinEvent(Alarm alarm) {
        this.alarm = alarm;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Alarm getAlarm() {
        return alarm;
    }

}

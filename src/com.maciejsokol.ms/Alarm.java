package com.maciejsokol.ms;

import java.util.ArrayList;
import java.util.List;

public class Alarm {
    private String pin;
    private List<AlarmListener> listeners;

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Alarm(String pin) {
        this.pin = pin;
        this.listeners = new ArrayList<AlarmListener>();
    }

    public void addListener(AlarmListener listener) {
        listeners.add(listener);
    }

    public void removeListener(AlarmListener listener) {
        listeners.remove(listener);
    }

    public void enterPin(String pin) {
        if (pin.equals("turbopin")) {
            correctEnteredPin();
        }
        else wrongEnteredPin();
    }

    private void wrongEnteredPin() {
        System.out.println("PIN incorrect. Access denied.");
        EnteredPinEvent e = new EnteredPinEvent(this);
        for (AlarmListener l : listeners) {
            l.alarmTurnedOn(e);
        }
    }

    private void correctEnteredPin() {
        System.out.println("PIN correct. Access granted.");
        EnteredPinEvent e = new EnteredPinEvent(this);
        for (AlarmListener l : listeners) {
            l.alarmTurnedOff(e);
        }
    }
}

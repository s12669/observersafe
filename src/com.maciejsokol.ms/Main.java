package com.maciejsokol.ms;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter PIN");

        Scanner scan = new Scanner(System.in);
        String pin = scan.nextLine();

        SoundAlert soundAlert = new SoundAlert();
        Police police = new Police();
        Bars bars = new Bars();
        Dogs dogs = new Dogs();
        Alarm alarm = new Alarm("alert");

        alarm.addListener(soundAlert);
        alarm.addListener(police);
        alarm.addListener(bars);
        alarm.addListener(dogs);
        alarm.setPin(pin);
        alarm.enterPin(pin);
    }

}

package com.maciejsokol.ms;

public class Dogs implements AlarmListener {

    @Override
    public void alarmTurnedOn(EnteredPinEvent e) {
        System.out.println("Dogs out: " + e.getAlarm());
    }

    @Override
    public void alarmTurnedOff(EnteredPinEvent e) {
        System.out.println("Dogs in: " + e.getAlarm());
    }
}

package com.maciejsokol.ms;

public class SoundAlert implements AlarmListener {
    
    @Override
    public void alarmTurnedOn(EnteredPinEvent e) {
        System.out.println("Turned on alarm: " + e.getAlarm());
    }

    @Override
    public void alarmTurnedOff(EnteredPinEvent e) {
        System.out.println("Turned off alarm: " + e.getAlarm());
    }
}

package com.maciejsokol.ms;

public interface AlarmListener {
    public void alarmTurnedOn(EnteredPinEvent e);
    public void alarmTurnedOff(EnteredPinEvent e);
}

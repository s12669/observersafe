package com.maciejsokol.ms;

public class Bars implements AlarmListener {

    @Override
    public void alarmTurnedOn(EnteredPinEvent e) {
        System.out.println("Bars down: " + e.getAlarm());
    }

    @Override
    public void alarmTurnedOff(EnteredPinEvent e) {
        System.out.println("Bars up: " + e.getAlarm());
    }
}

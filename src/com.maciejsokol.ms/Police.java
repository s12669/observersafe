package com.maciejsokol.ms;

public class Police implements AlarmListener {

    @Override
    public void alarmTurnedOn(EnteredPinEvent e) {
        System.out.println("Police called: " + e.getAlarm());
    }

    @Override
    public void alarmTurnedOff(EnteredPinEvent e) {
        System.out.println("Police not called: " + e.getAlarm());
    }
}
